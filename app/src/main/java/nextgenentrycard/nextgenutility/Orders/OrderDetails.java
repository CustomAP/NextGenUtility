package nextgenentrycard.nextgenutility.Orders;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.github.jorgecastilloprz.FABProgressCircle;
import com.rengwuxian.materialedittext.MaterialEditText;

import org.json.JSONObject;

import java.util.HashMap;

import nextgenentrycard.nextgenutility.Models.OrderItem;
import nextgenentrycard.nextgenutility.PicDailog;
import nextgenentrycard.nextgenutility.R;

/**
 * Created by Ashish Pawar(ashishpawar2015.ap@gmail.com) on 5/4/18.
 */

public class OrderDetails extends AppCompatActivity {
    TextView tvCustomerName, tvPaymentMode, tvOrderType, tvApartmentName, tvFlatNum, tvWing, tvAmount;
    MaterialEditText etAmount;
    FloatingActionButton fabDone;
    FABProgressCircle fpcDone;
    ImageView ivOrder;

    OrderItem orderItem;

    AQuery aQuery;

    String uploadAmountForOrder = "http://www.nextgenentrycard.com/index.php/Utilities/uploadAmountForOrder";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_details);
        initialize();
    }

    private void initialize() {
        tvCustomerName = findViewById(R.id.tvCustomerName_orderDetails);
        tvPaymentMode = findViewById(R.id.tvPaymentMode_orderDetails);
        tvOrderType = findViewById(R.id.tvOrderType_orderDetails);
        tvApartmentName = findViewById(R.id.tvApartmentName_orderDetails);
        tvFlatNum = findViewById(R.id.tvFlatNum_orderDetails);
        tvWing = findViewById(R.id.tvWing_orderDetails);
        etAmount = findViewById(R.id.etAmount_orderDetails);
        fabDone = findViewById(R.id.fabDone);
        tvAmount = findViewById(R.id.tvAmount_orderDetails);
        fpcDone = findViewById(R.id.fpcUpload_addOrderActivity);
        ivOrder = findViewById(R.id.ivOrder_orderDetails);

        orderItem = (OrderItem) getIntent().getSerializableExtra("orderItem");

        tvCustomerName.setText(orderItem.getCustomerName());
        tvPaymentMode.setText(orderItem.getPaymentMode());
        tvOrderType.setText(orderItem.getOrderType());
        tvApartmentName.setText(orderItem.getApartmentName());
        tvFlatNum.setText(orderItem.getFlatNo());
        tvWing.setText(orderItem.getWing());

        aQuery = new AQuery(getApplicationContext());

        if(orderItem.getAmount() != 0)
        fabDone.setImageResource(R.drawable.call);

        if(orderItem.getAmount() != 0){
            tvAmount.setVisibility(View.VISIBLE);
            etAmount.setVisibility(View.GONE);
            tvAmount.setText("Rs " + orderItem.getAmount() + "");
        }

        Glide.with(getApplicationContext()).load("http://www.nextgenentrycard.com/orders/"+orderItem.getOrderID()+".JPG").asBitmap().diskCacheStrategy(DiskCacheStrategy.ALL).into(ivOrder);


        ivOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getApplicationContext(), PicDailog.class);
                i.putExtra("orderID", orderItem.getOrderID());
                startActivity(i);
            }
        });

        fabDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (orderItem.getAmount() == 0) {
                    if (!etAmount.getText().toString().equals("")) {
                        fpcDone.show();

                        SharedPreferences sharedPreferences = getSharedPreferences("LoginDetails", Context.MODE_PRIVATE);
                        String sessionID = sharedPreferences.getString("sessionID", null);

                        HashMap<String, Object> amountParams = new HashMap<>();
                        amountParams.put("sessionID", sessionID);
                        amountParams.put("orderID", orderItem.getOrderID());
                        amountParams.put("amount", etAmount.getText().toString());


                        aQuery.ajax(uploadAmountForOrder, amountParams, JSONObject.class, new AjaxCallback<JSONObject>() {
                            @Override
                            public void callback(String url, JSONObject object, AjaxStatus status) {
                                int result = 0;
                                if (object != null) {
                                    try {
                                        Log.d("upload amount", "object != null");
                                        result = object.getInt("result");
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                } else {
                                    Toast.makeText(getApplicationContext(), "Failed to send Amount", Toast.LENGTH_SHORT).show();
                                    Log.d("upload amount", "object = null status : " + status.getCode());
                                    fpcDone.hide();
                                }

                                if (status.getCode() == 200) {
                                    if (result == 0) {
                                        fpcDone.hide();
                                        orderItem.setAmount(Integer.parseInt(etAmount.getText().toString()));
                                        fabDone.setImageResource(R.drawable.call);
                                        tvAmount.setVisibility(View.VISIBLE);
                                        tvAmount.setText(orderItem.getAmount() + "");
                                        etAmount.setVisibility(View.GONE);
                                    }
                                }
                            }
                        });
                    } else {
                        Toast.makeText(getApplicationContext(), "Enter amount first", Toast.LENGTH_SHORT).show();
                    }
                }else{
                    Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" +orderItem.getMobNum()));
                    startActivity(intent);
                }
            }
        });
    }
}
