package nextgenentrycard.nextgenutility.Orders;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import nextgenentrycard.nextgenutility.Models.OrderItem;
import nextgenentrycard.nextgenutility.R;

/**
 * Created by Ashish Pawar(ashishpawar2015.ap@gmail.com) on 31/3/18.
 */

public class OrderAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private ArrayList<OrderItem> orderItems;
    private Context context;

    public OrderAdapter(ArrayList<OrderItem> orderItems){
        this.orderItems = orderItems;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.order_single_item, parent, false);
        return new OrderHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        final OrderHolder orderHolder = (OrderHolder)holder;
        context = orderHolder.tvApartmentName.getContext();

        orderHolder.tvApartmentName.setText(orderItems.get(orderHolder.getAdapterPosition()).getApartmentName());
        orderHolder.tvCustomerName.setText(orderItems.get(orderHolder.getAdapterPosition()).getCustomerName());

        orderHolder.llOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(context, OrderDetails.class);
                i.putExtra("orderItem", orderItems.get(orderHolder.getAdapterPosition()));
                context.startActivity(i);
            }
        });
    }

    @Override
    public int getItemCount() {
        return orderItems.size();
    }

    public class OrderHolder extends RecyclerView.ViewHolder {
        private TextView tvCustomerName, tvApartmentName;
        LinearLayout llOrder;

        private OrderHolder(View itemView) {
            super(itemView);
            tvCustomerName = itemView.findViewById(R.id.tvCustomerName_orderSingleItem);
            tvApartmentName = itemView.findViewById(R.id.tvApartmentName_orderSingleItem);
            llOrder = itemView.findViewById(R.id.llOrderSingleItem);
        }
    }

    public void update(ArrayList<OrderItem> orderItems){
        this.orderItems = orderItems;
        this.notifyDataSetChanged();
    }
}
