package nextgenentrycard.nextgenutility.Orders;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.yarolegovich.lovelydialog.LovelyProgressDialog;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import nextgenentrycard.nextgenutility.AboutActivity;
import nextgenentrycard.nextgenutility.Models.OrderItem;
import nextgenentrycard.nextgenutility.Orders.OrderAdapter;
import nextgenentrycard.nextgenutility.R;

public class MainActivity extends AppCompatActivity {
    RecyclerView rvOrders;
    OrderAdapter orderAdapter;
    ArrayList<OrderItem> orderItems;
    AQuery aQuery;
    CardView cvNoOrders;

    Dialog orderProgress;

    String getOrders = "http://www.nextgenentrycard.com/index.php/Utilities/getOrdersForUtility";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setTitle("Orders");
        initialize();
    }

    private void initialize() {
        rvOrders = findViewById(R.id.rvOrders_mainActivity);
        cvNoOrders = findViewById(R.id.cvNoOrders);

        orderItems = new ArrayList<>();
        aQuery = new AQuery(getApplicationContext());

        orderAdapter = new OrderAdapter(orderItems);
        rvOrders.setAdapter(orderAdapter);
        rvOrders.setItemAnimator(new DefaultItemAnimator());
        rvOrders.setLayoutManager(new LinearLayoutManager(getApplicationContext()));


        orderProgress = new LovelyProgressDialog(this)
                .setTopColorRes(R.color.colorPrimary)
                .setIcon(R.drawable.wait)
                .setCancelable(true)
                .setTitle("Getting Orders")
                .show();

//        getOrders();
    }

    private void getOrders() {
        SharedPreferences sharedPreferences = getSharedPreferences("LoginDetails", Context.MODE_PRIVATE);
        String sessionID = sharedPreferences.getString("sessionID", null);

        HashMap<String, Object> orderParams = new HashMap<>();
        orderParams.put("sessionID", sessionID);

        aQuery.ajax(getOrders, orderParams, JSONObject.class, new AjaxCallback<JSONObject>(){
            @Override
            public void callback(String url, JSONObject object, AjaxStatus status) {
                int num;
                String orders;

                int orderID, userID, entityID, amount;
                String userName, date, time, paymentMode, mobNum, orderType, flatNum, wing, entityName;

                if(object != null){
                    try{
                        Log.d("getorders", "object != null");
                        orders = object.getString("orders");
                        num = object.getInt("num");

                        Log.d("num", num+"");

                        JSONObject reader = new JSONObject(orders);

                        for(int i = 0; i < num; i++){
                            JSONObject obj = reader.getJSONObject(String.valueOf(i));

                            orderID = obj.getInt("orderID");
                            userID = obj.getInt("userID");
                            entityID = obj.getInt("entityID");
                            amount = obj.getInt("amount");
                            userName = obj.getString("userName");
                            entityName = obj.getString("entityName");
                            date = obj.getString("date");
                            time = obj.getString("time");
                            paymentMode = obj.getString("paymentMode");
                            orderType = obj.getString("orderType");
                            mobNum = obj.getString("userMobNum");
                            flatNum = obj.getString("flatNum");
                            wing = obj.getString("wing");

                            orderItems.add(new OrderItem(orderID, entityID, userID, amount, userName, entityName, wing, flatNum, orderType, paymentMode, date, time, mobNum));
                        }
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }else{
                    Log.d("getorders", "object = null status:" + status.getCode());
                    orderProgress.dismiss();
                    Toast.makeText(getApplicationContext(), "Failed to get Orders", Toast.LENGTH_SHORT).show();
                }

                if(status.getCode() == 200){
                    if(orderItems.size() != 0)
                        cvNoOrders.setVisibility(View.GONE);
                    orderAdapter.update(orderItems);
                    orderProgress.dismiss();
                }
            }
        });
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
        orderItems.clear();
        getOrders();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.menu_about) {
            Intent i = new Intent(getApplicationContext(), AboutActivity.class);
            getApplicationContext().startActivity(i);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
