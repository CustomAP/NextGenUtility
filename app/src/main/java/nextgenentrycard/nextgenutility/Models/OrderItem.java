package nextgenentrycard.nextgenutility.Models;

import java.io.Serializable;

/**
 * Created by Ashish Pawar(ashishpawar2015.ap@gmail.com) on 31/3/18.
 */

public class OrderItem implements Serializable{
    private int orderID, apartmentID, userID, amount;
    private String customerName, apartmentName, wing, flatNo, orderType, paymentMode, date, time, mobNum;

    public int getOrderID() {
        return orderID;
    }

    public int getApartmentID() {
        return apartmentID;
    }

    public String getCustomerName() {
        return customerName;
    }

    public String getApartmentName() {
        return apartmentName;
    }

    public String getWing() {
        return wing;
    }

    public String getFlatNo() {
        return flatNo;
    }


    public String getOrderType() {
        return orderType;
    }

    public String getPaymentMode() {
        return paymentMode;
    }

    public int getUserID() {
        return userID;
    }

    public int getAmount() {
        return amount;
    }

    public String getDate() {
        return date;
    }

    public String getTime() {
        return time;
    }

    public String getMobNum() {
        return mobNum;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public OrderItem(int orderID, int apartmentID, int userID, int amount, String customerName, String apartmentName, String wing, String flatNo, String orderType, String paymentMode, String date, String time, String mobNum) {

        this.userID = userID;
        this.amount = amount;
        this.date = date;
        this.time = time;
        this.mobNum = mobNum;
        this.orderID = orderID;
        this.apartmentID = apartmentID;
        this.customerName = customerName;
        this.apartmentName = apartmentName;
        this.wing = wing;
        this.flatNo = flatNo;
        this.orderType = orderType;
        this.paymentMode = paymentMode;
    }
}
