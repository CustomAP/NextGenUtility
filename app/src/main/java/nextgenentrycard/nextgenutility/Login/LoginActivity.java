package nextgenentrycard.nextgenutility.Login;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.onesignal.OSPermissionSubscriptionState;
import com.onesignal.OneSignal;
import com.rengwuxian.materialedittext.MaterialEditText;
import com.wang.avi.AVLoadingIndicatorView;

import org.json.JSONObject;

import java.util.HashMap;

import de.greenrobot.event.EventBus;
import nextgenentrycard.nextgenutility.EventBus.OTPSentEvent;
import nextgenentrycard.nextgenutility.EventBus.OTPVerifiedEvent;
import nextgenentrycard.nextgenutility.Orders.MainActivity;
import nextgenentrycard.nextgenutility.R;

/**
 * Created by Ashish Pawar(ashishpawar2015.ap@gmail.com) on 31/3/18.
 */

public class LoginActivity extends AppCompatActivity {
    MaterialEditText etStoreName, etProprietorName, etMobNum, etSecondaryMobNum, etOTP, etGSTNum;
    TextView bSendOTP, bLogin;
    AVLoadingIndicatorView aviLogin;
    CheckBox cbDelivery;
    String mobileNum;
    ImageView ivDelete;
    TextView tvCountDown;
    String sessionID, otpString;
    AQuery aQuery;

    String utilityLogin = "http://www.nextgenentrycard.com/index.php/Login/utilityLogin";
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        EventBus.getDefault().register(this);
        initialize();
    }

    private void initialize() {
        etSecondaryMobNum = findViewById(R.id.etSecondaryNum_Login);
        etStoreName = findViewById(R.id.etStoreName_login);
        etProprietorName = findViewById(R.id.etProprietorName_login);
        etMobNum = findViewById(R.id.etMobileNum_Login);
        etOTP = findViewById(R.id.etOTP_login);
        etGSTNum = findViewById(R.id.etGSTNum_login);
        bSendOTP = findViewById(R.id.bSendOtp_login);
        bLogin = findViewById(R.id.bSubmit_Login);
        aviLogin = findViewById(R.id.aviLogin);
        ivDelete = findViewById(R.id.ivDelete_Login);
        tvCountDown = findViewById(R.id.tvCountDown_Login);
        cbDelivery = findViewById(R.id.cbDelivery_Login);

        aQuery = new AQuery(getApplicationContext());

        final OTPServerConnection otpServerConnection = new OTPServerConnection(getApplicationContext());

        bSendOTP.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mobileNum = etMobNum.getText().toString();
                if (mobileNum.length() != 10) {
                    Toast.makeText(getApplicationContext(), "Mobile Number should be of 10 Characters", Toast.LENGTH_SHORT).show();
                } else {
                    etMobNum.setFocusable(false);
                    etMobNum.setClickable(false);
                    etMobNum.setFocusableInTouchMode(false);

                    aviLogin.setVisibility(View.VISIBLE);
                    aviLogin.show();
                    otpServerConnection.sendOTP(mobileNum);
                }
            }
        });

        ivDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                etMobNum.setClickable(true);
                etMobNum.setFocusable(true);
                etMobNum.setFocusableInTouchMode(true);

                bSendOTP.setEnabled(true);
                bSendOTP.setFocusable(true);
                bSendOTP.setFocusableInTouchMode(true);
                bSendOTP.setClickable(true);

                bLogin.setEnabled(true);
                bLogin.setFocusableInTouchMode(true);
                bLogin.setFocusable(true);
                bLogin.setClickable(true);

                etMobNum.setText("");
                tvCountDown.setVisibility(View.GONE);

                sessionID = null;
                otpString = null;
                mobileNum = null;

                ivDelete.setVisibility(View.GONE);
            }
        });

        bLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!etStoreName.getText().toString().equals("")) {
                    if (!etProprietorName.getText().toString().equals("")) {
                        if(!etSecondaryMobNum.getText().toString().equals("")) {
                            if(!etGSTNum.getText().toString().equals("")) {
                                if (sessionID != null) {
                              /*
                                Verify if the otp matches

                                */
                                    otpString = etOTP.getText().toString();
                                    aviLogin.setVisibility(View.VISIBLE);
                                    aviLogin.show();
                                    bLogin.setEnabled(false);
                                    bLogin.setFocusable(false);
                                    bLogin.setClickable(false);
                                    otpServerConnection.verifyOTP(sessionID, otpString);
                                } else {
                                    Toast.makeText(getApplicationContext(), "Please verify mobile number first", Toast.LENGTH_SHORT).show();
                                }
                            }else{
                                Toast.makeText(getApplicationContext(), "Please Enter GST Number", Toast.LENGTH_SHORT).show();
                            }
                        }else{
                            Toast.makeText(getApplicationContext(), "Please Enter Secondary Mobile Number", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(getApplicationContext(), "Please enter Proprietor Name", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(getApplicationContext(), "Please enter Store Name", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    public void onEvent(OTPSentEvent otpSentEvent) {
        boolean otpSuccess = otpSentEvent.isOtpSuccess();

        aviLogin.hide();
        aviLogin.setVisibility(View.GONE);
        if (otpSuccess) {
            sessionID = otpSentEvent.getSessionID();
            mobileNum = otpSentEvent.getMobNum();
            Log.d("otp sent", "success");
            Log.d("mobNum", mobileNum);
            Log.d("sessionID", sessionID);

            etMobNum.setClickable(false);
            etMobNum.setFocusable(false);
            etMobNum.setFocusableInTouchMode(false);

            bSendOTP.setEnabled(false);
            bSendOTP.setFocusable(false);
            bSendOTP.setFocusableInTouchMode(false);
            bSendOTP.setClickable(false);

            tvCountDown.setVisibility(View.VISIBLE);
            new CountDownTimer(60000, 1000) {

                public void onTick(long millisUntilFinished) {
                    tvCountDown.setText("OTP sent successfully\nTry again in 00:" + millisUntilFinished / 1000);
                }

                public void onFinish() {
                    tvCountDown.setVisibility(View.GONE);

                    etMobNum.setFocusable(true);
                    etMobNum.setClickable(true);
                    etMobNum.setFocusableInTouchMode(true);

                    bSendOTP.setEnabled(true);
                    bSendOTP.setFocusable(true);
                    bSendOTP.setFocusableInTouchMode(true);
                    bSendOTP.setClickable(true);

                    ivDelete.setVisibility(View.GONE);
                }
            }.start();
        } else {
            etMobNum.setFocusable(true);
            etMobNum.setClickable(true);
            etMobNum.setFocusableInTouchMode(true);

            bSendOTP.setEnabled(true);
            bSendOTP.setFocusable(true);
            bSendOTP.setFocusableInTouchMode(true);
            bSendOTP.setClickable(true);

            ivDelete.setVisibility(View.GONE);
        }
    }

    public void onEvent(OTPVerifiedEvent otpVerifiedEvent) {

        boolean isOTPVerified = otpVerifiedEvent.isOTPVerified();

        if (isOTPVerified) {
            aviLogin.hide();
            aviLogin.setVisibility(View.GONE);

            SharedPreferences sharedPreferences = getSharedPreferences("LoginDetails", MODE_PRIVATE);
            final SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putString("mobileNum", mobileNum);
            editor.apply();

//            mobNum = etMobileNumSignUp.getText().toString();   ///comment this later only for testing

            /*
                login the user
             */

            OSPermissionSubscriptionState status = OneSignal.getPermissionSubscriptionState();


            HashMap<String, Object> loginParams = new HashMap<>();
            loginParams.put("mobNum" , mobileNum);
            loginParams.put("proprietorName", etProprietorName.getText().toString());
            loginParams.put("storeName" , etStoreName.getText().toString());
            loginParams.put("secondaryMobNum", etSecondaryMobNum.getText().toString());
            loginParams.put("gstNum", etGSTNum.getText().toString());
            loginParams.put("delivery", cbDelivery.isChecked()?1:0);
            loginParams.put("playerID", status.getSubscriptionStatus().getUserId());


            aQuery.ajax(utilityLogin, loginParams, JSONObject.class, new AjaxCallback<JSONObject>(){
                @Override
                public void callback(String url, JSONObject object, AjaxStatus status) {
                    String sessionID = null;
                    if(object != null){
                        Log.d("login", "object != null");
                        try{
                            sessionID = object.getString("sessionID");
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }else{
                        Toast.makeText(getApplicationContext(), "Failed to login", Toast.LENGTH_SHORT).show();
                        Log.d("login", "object = null status:" + status.getCode());
                    }

                    if(status.getCode() == 200){
                        editor.putString("storeName", etStoreName.getText().toString());
                        editor.putString("proprietorName", etProprietorName.getText().toString());
                        editor.putString("secondaryMobNum", etSecondaryMobNum.getText().toString());
                        editor.putString("gstNum", etGSTNum.getText().toString());
                        editor.putBoolean("isLoggedIn", true);
                        editor.putString("sessionID", sessionID);

                        editor.apply();

                        Intent i =  new Intent(getApplicationContext(), MainActivity.class);
                        getApplicationContext().startActivity(i);
                    }
                }
            });
        } else {
            Toast.makeText(this, "OTP verification failed", Toast.LENGTH_SHORT).show();
            aviLogin.setVisibility(View.GONE);
            aviLogin.hide();
            bLogin.setClickable(true);
            bLogin.setFocusable(true);
            bLogin.setFocusableInTouchMode(true);
            bLogin.setEnabled(true);
        }


    }

    @Override
    protected void onPause() {
        super.onPause();
        SharedPreferences preferences = getSharedPreferences("LoginDetails", MODE_PRIVATE);
        if (preferences.getBoolean("isLoggedIn", false))
            finish();
    }
}
