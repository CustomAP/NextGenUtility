package nextgenentrycard.nextgenutility;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

/**
 * Created by Ashish Pawar(ashishpawar2015.ap@gmail.com) on 2/11/17.
 */

public class PicDailog extends Activity {
    ImageView ivPic;
    int orderID;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_pic);
        initialize();
    }

    private void initialize() {
        /*
            initialize views
         */
        ivPic = (ImageView) findViewById(R.id.ivPic);

        orderID = getIntent().getIntExtra("orderID", 0);

        Glide.with(getApplicationContext()).load("http://www.nextgenentrycard.com/orders/"+orderID+".JPG").asBitmap().diskCacheStrategy(DiskCacheStrategy.ALL).into(ivPic);

    }
}
